#pragma once

/**	@file trie_imp.hpp
	@author Joshua Wright
	@date 2017-06-11
	@version 1.0.0
	@note Developed for C++11/vc14
	@breif trie<T> template implementations.
	*/

#include "trie.hpp"

#ifdef _DEBUG 
template<typename T>
std::vector<std::string> trie<T>::_log;
#endif

template<typename T>
trie<T>::trie() : root(new node) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr,"root node was not constructed")
#endif
} //  trie()


template<typename T>
trie<T>::trie(trie && other) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
	TRIE_ASSERT_DUMP_LOG(other.root != nullptr, "other must have a root")
#endif

	root = std::move(other.root);
	other.root = new node();

#ifdef _DEBUG
	TRIE_ASSERT_DUMP_LOG(other.root != nullptr, "other.root node was not constructed")
#endif
} // trie(trie && other)


template<typename T>
trie<T>::trie(trie const & other) : root(new node) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
	TRIE_ASSERT_DUMP_LOG(other.root != nullptr, "other must have a root")
#endif
	for (auto& it : other) {
		insert(value_type(it.first, it.second));
	}
} // trie(trie const & other) 


template<typename T>
template<class InputIterator>
trie<T>::trie(InputIterator first, InputIterator last) : root(new node) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
#endif
	insert(first, last);
}// :trie(InputIterator first, InputIterator last) 


template<typename T>
trie<T>::trie(std::initializer_list<value_type> const & init) : root(new node) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
#endif
	for (auto e : init)
		insert(e);
} // trie(std::initializer_list<value_type> const & init)


template<typename T>
trie<T>::~trie() {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
#endif
	clear();
	delete root;
} // ~trie()


template<typename T>
trie<T>& trie<T>::operator=(trie && rhs) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr ,"rhs must have a root")
#endif
	
	clear();
	delete root;
	root = std::move(rhs.root);
	rhs.root = new node();

#ifdef _DEBUG 
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr , "rhs.root node was not constructed")
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not moved")
#endif
	return *this;
} // operator=(trie && rhs)


template<typename T>
trie<T>& trie<T>::operator=(trie const & rhs) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr ,"root node must not be nullptr")
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr ,"rhs must have a root")
#endif

	clear();
	for (auto& it : rhs) {
		insert(value_type(it.first, it.second));
	}
	return *this;
} // operator=(trie const & rhs)


template<typename T>
typename trie<T>::node*	trie<T>::_insert(node* n,char const& c) {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node was not constructed")
	TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not insert from an nullptr node")
#endif
	byte b = trie::_translate(c);

	node* child = n->children[b.first];
	node* first_nibble_node;
	node* secocnd_nibble_node = new node(b.second);
	secocnd_nibble_node->is_second_nibble = true;

	if (child) {
		child->children[b.second] = secocnd_nibble_node;
		secocnd_nibble_node->parent = child;
	}
	else {
		first_nibble_node = new node(b.first);
		n->children[b.first] = first_nibble_node;
		first_nibble_node->parent = n;
		first_nibble_node->children[b.second] = secocnd_nibble_node;
		secocnd_nibble_node->parent = first_nibble_node;
	}
	return secocnd_nibble_node;
} // _insert(node* n, char c)


template<typename T>
typename trie<T>::node* trie<T>::_get(node * n, char const& c) {
#ifdef _DEBUG 
	TRIE_LOG;
#endif
	
	byte b = trie::_translate(c);

	node* first_nibble_node = n->children[b.first];

	if (first_nibble_node == nullptr)
		return nullptr;

	node* second_nibble_node = first_nibble_node->children[b.second];

	return second_nibble_node;
} // _get(node * n, char c)


template<typename T>
inline bool trie<T>::_equal_from_second_nibble(node* n, char const& c) {
#ifdef _DEBUG 
	TRIE_LOG;
#endif

	if (trie::_translate(n->parent->nibble, n->nibble) == c)
		return true;
	return false;
} // _equal_from_second_nibble(node* n, char c)


template<typename T>
bool trie<T>::operator==(trie const & rhs) const {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr ,"root node must not be nullptr")
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr , "rhs.root node must not be nullptr")
#endif
	
	if (size() != rhs.size())
		return false;

	auto this_it = cbegin();
	auto rhs_it = rhs.cbegin();
	while (this_it != end()) {
		if (*rhs_it++ != *this_it++)
			return false;
	}
	return true;
} // operator==(trie const & rhs)


template<typename T>
bool trie<T>::operator>(trie const& rhs) const {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr ,"root node must not be nullptr")
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr , "rhs.root node must not be nullptr")
#endif

	if (size() > rhs.size())
		return true;

	auto this_it = cbegin();
	auto rhs_it = rhs.cbegin();
	while (this_it != end()) {
		if (*rhs_it > *this_it)
			return false;
		if (*this_it++ > *rhs_it++)
			return true;
	}
	return false;
} // operator>(trie const& rhs) 


template<typename T>
trie<T>& trie<T>::operator=(std::initializer_list<value_type> const & init) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
#endif
	clear();
	for (auto e : init)
		insert(e);

	return *this;
} // operator=(std::initializer_list<value_type> const & init)


template<typename T>
typename trie<T>::mapped_type& trie<T>::operator[](key_type const& key) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
#endif

	iterator it = find(key);
	if (it.currentNode == nullptr) {
		insert_return_type result = insert(value_type(key, mapped_type()));

#ifdef _DEBUG
		TRIE_ASSERT_DUMP_EVERYTHING(result.second == true ,"value must be inserted")
#endif

		return result.first.currentNode->value;
	}
	if (!it.currentNode->is_terminal) {
		it.currentNode->is_terminal = true;

		if (key.empty() || key.length() == 1)
			++root->num_terminal_descendants;
		else {
			++(it.currentNode->parent->num_terminal_descendants);
			++root->num_terminal_descendants;
		}
#ifdef _DEBUG
		it.currentNode->key = key;
#endif
	}
	return it.currentNode->value;

} // operator[](key_type const& key)


template<typename T>
void trie<T>::swap(trie<T>& rhs) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
	TRIE_ASSERT_DUMP_LOG(rhs.root != nullptr , "rhs.root must not be nullptr")
#endif
	std::swap(root, rhs.root);
} // swap(trie<T>& rhs)


template<typename T>
typename trie<T>::size_type trie<T>::count(key_type const& key) const{
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
#endif

	if (key.empty())
		return root->num_terminal_descendants;

	node* result = _find_node(key);
	if (result == nullptr)
		return 0;
	if (result->is_terminal) {
		return result->num_terminal_descendants + 1;
	}
	return result->num_terminal_descendants;

} // count(key_type const& key)


template<typename T>
typename trie<T>::node* trie<T>::_find_node(key_type const& key) const {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
#endif

	if (key.empty())
		return root;

	int level = 1;
	node* result = nullptr;
	node* child = _get(root, key.at(0));

	if (child != nullptr) {
		_find_node_loop_recursion(child, key, level, result);
		return result;
	}
	return result;
} // _find_node(key_type const& key)


template<typename T>
void trie<T>::_find_node_loop_recursion(node*& n, key_type const& key, int& level, node*& result) const{
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr , "root node must not be nullptr")
#endif
	
	if (key.size() == level) {
		result = n;
		return;
	}
	node* child = _get(n, key.at(level));

	if (child != nullptr) {
		_find_node_loop_recursion(child, key, ++level, result);
		return;
	}
	// Key not in trie

} // end find_node_loop_recursion


template<typename T>
typename trie<T>::iterator trie<T>::find(key_type const& key) const{
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	
	int position = 0;
	iterator result = iterator(this, nullptr);

	if (key.empty())
		return iterator(this,root);

	int level = 1;

	node* child = _get(root, key.at(0));

	if (child != nullptr) {
		_find_loop_recursion(child, key, level, result);
		return result;
	}
	return result;
} // find(key_type const& key)


template<typename T>
void trie<T>::_find_loop_recursion(node* n, key_type const& key, int& level, iterator& result) const {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	
	if (key.size() == level) {
		result = iterator(this, n);
		return;
	}

	node* child = _get(n, key.at(level));

	if (child != nullptr) {
		_find_loop_recursion(child, key, ++level, result);
		return;
	}
	// Key not in trie

} //  end find_loop_recursion


template<typename T>
typename trie<T>::insert_return_type trie<T>::insert(value_type&& toBeInserted) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	insert_return_type result;
	int level = 0;

	// test for insert into root
	if (toBeInserted.first.empty()) {
		result.second = false;
		result.first = iterator(this, root, toBeInserted.first);
		if (root->is_terminal == false) {
			root->is_terminal = true;
			root->value = std::move(toBeInserted.second);
			++root->num_terminal_descendants;
#ifdef _DEBUG
			root->key = "";
#endif
		}
		return result;
	}

	node* child = _get(root, toBeInserted.first.at(0));
	//node* child = root->children[get_ascii(toBeInserted.first.at(0))];

	if (child == nullptr) {
		// insert from root
		_insert_from_root(root, level, std::move(toBeInserted), result);
		return result;
	}

	// insert from below root
	_insert_from_under_root(child, level, std::move(toBeInserted), result);
	return result;
} // insert(value_type&& toBeInserted)


template<typename T>
typename trie<T>::insert_return_type trie<T>::insert(value_type const& toBeInserted) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	insert_return_type result;
	int level = 0;

	// test for insert into root
	if (toBeInserted.first.empty()) {
		result.second = false;
		result.first = iterator(this,root, toBeInserted.first);
		if (root->is_terminal == false) {
			root->is_terminal = true;
			root->value = toBeInserted.second;
			++root->num_terminal_descendants;
#ifdef _DEBUG
			root->key = "";
#endif
		}
		return result;
	}

	node* child = _get(root, toBeInserted.first.at(0));
	//node* child = root->children[get_ascii(toBeInserted.first.at(0))];

	if (child == nullptr) {
		// insert from root
		_insert_from_root(root, level, toBeInserted, result);
		return result;
	}

	// insert from below root
	_insert_from_under_root(child, level, toBeInserted, result);
	return result;
} // insert(value_type const& toBeInserted)


template<typename T>
void trie<T>::insert(std::initializer_list<value_type> const& init) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	for (auto e : init)
		insert(e);

} // insert(std::initializer_list<value_type> const& init)


template<typename T>
template<class InputIt>
void trie<T>::insert(InputIt first, InputIt last) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	while (first != last)
		insert(*first++);

} // insert(InputIt first, InputIt last)


template<typename T>
void trie<T>::_insert_from_root(node* n, int& level, value_type&& toBeInserted, insert_return_type& result) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	// incrementing counts
	++n->num_terminal_descendants;

	// checking if end of key has been reached 
	if (toBeInserted.first == toBeInserted.first.substr(0, level + 1)) {
		node* lastNode = _insert(n, toBeInserted.first.back());
		lastNode->value = std::move(toBeInserted.second);
		lastNode->is_terminal = true;
		result.second = true;
		result.first = iterator(this, lastNode, std::move(toBeInserted).first);
#ifdef _DEBUG
		lastNode->key = toBeInserted.first;
#endif
		return;
	}
	node* onTheWayNode = _insert(n, toBeInserted.first.at(level));
	_insert_from_root(onTheWayNode, ++level, std::move(toBeInserted), result);

} // _insert_from_root


template<typename T>
void trie<T>::_insert_from_under_root(node* n, int& level, value_type&& toBeInserted, insert_return_type& result) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	++level;

	// checking in key already exist 
	if (toBeInserted.first.size() == level) {
		result.second = false;
		result.first = iterator(this, n, toBeInserted.first);
		if (n->is_terminal == false) {
			n->is_terminal = true;
			n->value = std::move(toBeInserted.second);
			if (*n->parent == *root) {
				++root->num_terminal_descendants;
			}
			else {
				++n->parent->num_terminal_descendants;
				++root->num_terminal_descendants;
			}
#ifdef _DEBUG
			n->key = toBeInserted.first;
#endif
		}
		return;
	}

	node* child = _get(n, toBeInserted.first.at(level));

	if (child != nullptr) {
		if (trie::_equal_from_second_nibble(child, toBeInserted.first.at(level))) {
			_insert_from_under_root(child, level, std::move(toBeInserted), result);
			return;
		}
	}

	++n->num_terminal_descendants;

	// if key chunk dose exist make new branch 
	if (toBeInserted.first.size() > toBeInserted.first.substr(0, level + 1).size()) {
		node* notLastNode = _insert(n, toBeInserted.first.at(level));
		_insert_from_under_root(notLastNode, level, std::move(toBeInserted), result);
		return;
	}

	++root->num_terminal_descendants;

	node* lastNode = _insert(n, toBeInserted.first.back());
	lastNode->value = std::move(toBeInserted.second);
	lastNode->is_terminal = true;
	result.second = true;
	result.first = iterator(this, lastNode);
#ifdef _DEBUG
	lastNode->key = toBeInserted.first;
#endif
	return;
} // end insert_existing_loop_recursion

template<typename T>
void trie<T>::_insert_from_root(node* n, int& level, value_type const& toBeInserted, insert_return_type& result) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	// incrementing counts
	++n->num_terminal_descendants;

	// checking if end of key has been reached 
	if (toBeInserted.first == toBeInserted.first.substr(0, level + 1)) {
		node* lastNode = _insert(n, toBeInserted.first.back());
		lastNode->value = toBeInserted.second;
		lastNode->is_terminal = true;
		result.second = true;
		result.first = iterator(this, lastNode,toBeInserted.first);
#ifdef _DEBUG
		lastNode->key = toBeInserted.first;
#endif
		return;
	}

	node* onTheWayNode = _insert(n, toBeInserted.first.at(level));
	_insert_from_root(onTheWayNode, ++level, toBeInserted, result);

} // _insert_from_root


template<typename T>
void trie<T>::_insert_from_under_root(node* n, int& level, value_type const& toBeInserted, insert_return_type& result) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif
	++level;

	// checking in key already exist 
	if (toBeInserted.first.size() == level) {
		result.second = false;
		result.first = iterator(this, n, toBeInserted.first);
		if (n->is_terminal == false) {
			n->is_terminal = true;
#ifdef _DEBUG 
			n->key = toBeInserted.first;
#endif
			n->value = toBeInserted.second;
			if (*n->parent == *root) {
				++root->num_terminal_descendants;
			}
			else {
				++n->parent->num_terminal_descendants;
				++root->num_terminal_descendants;
			}
#ifdef _DEBUG
			n->key = toBeInserted.first;
#endif
		}
		return;
	}

	node* child = _get(n, toBeInserted.first.at(level));

	if (child != nullptr) {
		if (trie::_equal_from_second_nibble(child, toBeInserted.first.at(level))) {
			_insert_from_under_root(child, level, toBeInserted, result);
			return;
		}
	}

	++n->num_terminal_descendants;

	// if key chunk dose exist make new branch 
	if (toBeInserted.first.size() > toBeInserted.first.substr(0, level + 1).size()) {
		node* notLastNode = _insert(n, toBeInserted.first.at(level));
		_insert_from_under_root(notLastNode, level, toBeInserted, result);
		return;
	}

	++root->num_terminal_descendants;

	node* lastNode = _insert(n, toBeInserted.first.back());
	lastNode->value = toBeInserted.second;
	lastNode->is_terminal = true;
	result.second = true;
	result.first = iterator(this, lastNode);
#ifdef _DEBUG
	lastNode->key = toBeInserted.first;
#endif
	return;
} // insert_existing_loop_recursion


template<typename T>
void trie<T>::clear() {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be nullptr")
#endif

	_clear_loop_recursion(root);

	if(root->is_terminal)
		--root->num_terminal_descendants;

#ifdef _DEBUG
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root must not be deleted")
	TRIE_ASSERT_DUMP_EVERYTHING(size() == 0, "size must be 0")
#endif
} // clear()


template<typename T>
void trie<T>::_clear_loop_recursion(node* n) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root must not be deleted")
#endif
	for (int i = 0; i < n->children.size(); ++i) {
		if (n->children[i] != nullptr) {
			_clear_loop_recursion(n->children[i]);
			if (n->children[i]->is_terminal) {
				--root->num_terminal_descendants;
			}
			delete n->children[i];
			n->children[i] = nullptr;
		}
	}
#ifdef _DEBUG
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root must not be deleted")
#endif
} // _clear_loop_recursion(node* n)


template<typename T>
typename trie<T>::iterator trie<T>::erase(iterator const& it) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be erased")
#endif
	if (it.currentNode == nullptr)
		return it;

	if (*it.currentNode == *root) {
		return end();
	}

	iterator result(it);
	++result;

	if (*it.currentNode->parent == *root) {
		--root->num_terminal_descendants;
	}
	else {
		--(it.currentNode->parent->num_terminal_descendants);
		--root->num_terminal_descendants;
	}

	if (it.currentNode->num_terminal_descendants > 0) {
		it.currentNode->is_terminal = false;
		return result;
	}
	iterator parent(this, it.currentNode->parent);

	it.currentNode->parent->children[it.currentNode->nibble] = nullptr;
	delete (it.currentNode);
	it.currentNode = nullptr;


	_erase_loop_recursion(parent, result);

#ifdef _DEBUG 
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be deleted")
#endif

	return result;
} // erase(iterator const& it)


template<typename T>
void trie<T>::_erase_loop_recursion(iterator& it, iterator& result) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "root node must not be deleted")
#endif
	if (it.currentNode->has_children() == false)
		it.currentNode->num_terminal_descendants = 0;
	if (it.currentNode->has_children() || it.currentNode->is_terminal)
		return;

	if (it.currentNode->parent != nullptr) {
		iterator parent(this, it.currentNode->parent);
		it.currentNode->parent->children[it.currentNode->nibble] = nullptr;
		delete (it.currentNode);
		it.currentNode = nullptr;
		result = parent;
		_erase_loop_recursion(parent, result);
	}
	// at root 
} // _erase_loop_recursion(iterator& it, iterator& result)


template<typename T>
typename trie<T>::iterator trie<T>::erase(iterator const& beg_it, iterator const& end_it) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "trie must alway have a root")
#endif
	iterator current(beg_it);
	while (current != end_it) {
		current = erase(current);
	}
	return current;
}//  erase(iterator const& beg_it, iterator const& end_it)


template<typename T>
typename trie<T>::size_type trie<T>::erase(key_type const & key) {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "trie must alway have a root")
#endif

	node* toBeErased = _find_node(key);
	if (toBeErased == nullptr) {
		return 0;
	}

	erase(iterator(this, toBeErased));
	return 1;
} // erase(key_type const & key)


template<typename T>
typename trie<T>::iterator trie<T>::begin() {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "trie must alway have a root") 
#endif
	if (root->has_children() == false)
		return end();

	iterator it(this);
	it._forward_first_terminal_descendant(root, it.currentNode);
	return it;
} // begin()

template<typename T>
typename trie<T>::const_iterator trie<T>::cbegin() const {
#ifdef _DEBUG 
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(root != nullptr, "trie must alway have a root")
#endif
	if (root->has_children() == false)
		return end();

	const_iterator it(this);
	it._forward_first_terminal_descendant(root, it.currentNode);
	return it;
} // cbegin()


#if _DEBUG

template<typename T>
void trie<T>::_update_log(char * const& func, int const& line, char * const& file) {
	std::stringstream ss;
	ss << "Function: " << func << ", Line: " << line << ", File: " << file << "\n";
	_log.push_back(ss.str());
} // _update_log(char * func, int line, char * file)


template<typename T>
void trie<T>::dump_everything(char*  const& file) const {
	assert(root != nullptr && "root should never be nullptr");
	std::ofstream ofs(file);
	ofs << "Check Count: " << _log.size() << "\n";
	for (auto e : _log)
		ofs << e;

	ofs << "\nNodes:\n\nroot:\n" << root->_to_string();
	_dump_children(root, ofs);

	ofs.close();
} // _dump_eveything()


template<typename T>
void trie<T>::dump_log(char* const& file) const {
	assert(root != nullptr && "root should never be nullptr");
	std::ofstream ofs(file);
	ofs << "Check Count: " << _log.size() << "\n";
	for (auto e : _log)
		ofs << e;

	ofs.close();
} // _dump_eveything()


template<typename T>
void trie<T>::_dump_children(node* n, std::ofstream& ofs) const {
	assert(n != nullptr && "n should never be nullptr");
	for (int i = 0; i < n->children.size(); ++i) {
		if (n->children[i] != nullptr) {
			ofs << "\n" << n->children[i]->_to_string();
			_dump_children(n->children[i], ofs);
		}
	}
} // _dump_children(node* n, std::ofstream& ofs, std::string tab) 

#endif
