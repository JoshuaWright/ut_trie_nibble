#pragma once

/**	@file iterator_imp.hpp
@author Joshua Wright
@date 2017-06-11
@version 1.0.0
@note Developed for C++11/vc14
@breif trie<T>::iterator template implementations.
*/

#include "trie.hpp"


#ifdef _DEBUG
template<typename T>
std::vector<std::string> trie<T>::iterator::_log;
#endif


template<typename T>
trie<T>::base_iterator::base_iterator() {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::base_iterator::base_iterator(trie const* c, node* n, key_type const& k,_state const& s)
	: container(c), currentState(s),
	currentNode(n), accumulated(k) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::base_iterator::base_iterator(trie const* c, node* n, key_type const& k) 
	: container(c), currentNode(n), accumulated(k) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::base_iterator::base_iterator(trie const* c, node* n = nullptr) 
	: container(c), currentNode(n) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::base_iterator::~base_iterator() {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::iterator::iterator() {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::iterator::iterator(iterator const& it) 
	: base_iterator(it.container,it.currentNode,it.accumulated, it.currentState) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::iterator::iterator(trie const* c, node* n, key_type const& k) : base_iterator(c, n, k) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::iterator::iterator(trie const* c, node* n = nullptr) :base_iterator(c, n) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::iterator::~iterator(){
#ifdef _DEBUG
	TRIE_LOG;
#endif
	delete value;
}


template<typename T>
trie<T>::const_iterator::const_iterator() {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::const_iterator::const_iterator(const_iterator const& it) 
	: base_iterator(it.container, it.currentNode, it.accumulated, it.currentState) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::const_iterator::const_iterator(trie const* c, node* n, key_type const& k) : base_iterator(c, n, k) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::const_iterator::const_iterator(trie const* c, node* n = nullptr) :base_iterator(c, n) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
}

template<typename T>
trie<T>::const_iterator::~const_iterator() {
#ifdef _DEBUG
	TRIE_LOG;
#endif
	delete value;
}

template<typename T>
void trie<T>::iterator::_assign(iterator const& it) const {
#ifdef _DEBUG
	TRIE_LOG;
#endif
	if (value != nullptr) {
		currentNode->value = std::move(value->second);
		delete value;
		value = nullptr;
	}
	container = it.container;
	currentNode = it.currentNode;
	currentState = it.currentState;
	accumulated = it.accumulated;
}


template<typename T>
void trie<T>::const_iterator::_assign(const_iterator const& it) const{
#ifdef _DEBUG
	TRIE_LOG;
#endif
	if (value != nullptr) {
		delete value;
		value = nullptr;
	}
	container = it.container;
	currentNode = it.currentNode;
	currentState = it.currentState;
	accumulated = it.accumulated;
}


template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator=(iterator const& rhs) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
	if (value != nullptr) {
		currentNode->value = std::move(value->second);
		delete value;
		value = nullptr;
	}
	container = rhs.container;
	currentNode = rhs.currentNode;
	currentState = rhs.currentState;
	accumulated = rhs.accumulated;
	return *this;
}

template<typename T>
typename trie<T>::const_iterator& trie<T>::const_iterator::operator=(iterator const& rhs) {
#ifdef _DEBUG
	TRIE_LOG;
#endif
	if (value != nullptr) {
		delete value;
		value = nullptr;
	}
	container = rhs.container;
	currentNode = rhs.currentNode;
	currentState = rhs.currentState;
	accumulated = rhs.accumulated;
	return *this;
}


template<typename T>
void trie<T>::base_iterator::_forward_first_child(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		for (unsigned int i = 0; i < n->children.size(); ++i) {
			if (n->children[i] != nullptr) {
				result = n->children[i];
				if (result->is_second_nibble)
					accumulated += trie::_translate(n->nibble, result->nibble);
				return;
			}
		}
}

template<typename T>
void trie<T>::base_iterator::_backward_first_child(node *& n, node *& result)  const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		for (std::size_t i = n->children.size() - 1; i >= 0; --i) {
			if (n->children[i] != nullptr) {
				result = n->children[i];
				if (result->is_second_nibble)
					accumulated += trie::_translate(n->nibble, result->nibble);
				return;
			}
		}
}

template<typename T>
void trie<T>::base_iterator::_backward_first_sibling(node *& n, node*& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		if (!accumulated.empty() && n->is_second_nibble)
			accumulated.pop_back();

	for (int i = n->nibble - 1; i >= 0; --i) {
		if (n->parent->children[i] != nullptr) {
			result = n->parent->children[i];
			if (result->is_second_nibble)
				accumulated.push_back(trie::_translate(n->parent->nibble, result->nibble));
			return;
		}
	}
}

template<typename T>
void trie<T>::base_iterator::_forward_first_sibling(node *& n, node*& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		if (!accumulated.empty() && n->is_second_nibble)
			accumulated.pop_back();

	for (unsigned int i = n->nibble + 1; i < n->children.size(); ++i) {
		if (n->parent->children[i] != nullptr) {
			result = n->parent->children[i];
			if (result->is_second_nibble)
				accumulated.push_back(trie::_translate(n->parent->nibble, result->nibble));
			return;
		}
	}
}

template<typename T>
void trie<T>::base_iterator::_forward_first_terminal_descendant(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		if (n->is_terminal) {
			currentState = _state::down;
			result = n;
			return;
		}
	node* childNode = nullptr;

	_forward_first_child(n, childNode);

	if (childNode == nullptr) {
		return;
	}
	result = childNode;
	_forward_first_terminal_descendant(result, result);
}

template<typename T>
void trie<T>::base_iterator::_backward_first_terminal_descendant(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		if (n->has_children() == false) {
			result = n;
			return;
		}
	node* childNode = nullptr;

	_backward_first_child(n, childNode);

	if (childNode == nullptr) {
		return;
	}
	result = childNode;
	_backward_first_terminal_descendant(result, result);
}

template<typename T>
void trie<T>::base_iterator::_forward_execute(node *& n, node *& result)  const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		if (n->has_children() == true)
			_forward_down(n, result);
		else
			_forward_up(n, result);
}

template<typename T>
void trie<T>::base_iterator::_forward_down(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		// STOPPING CONDITION
		if (n->is_terminal && (currentState == _state::going_down)) {
			currentState = _state::down;
			result = n;
			return;
		}

	node* childNode = nullptr;
	_forward_first_child(n, childNode);

	if (childNode != nullptr) {
		// STOPPING CONDITION
		if (childNode->is_terminal == false) {
			_forward_down(childNode, result);
			return;
		}
		currentState = _state::going_up;
		result = childNode;
		return;
	}
}

template<typename T>
void trie<T>::base_iterator::_forward_up(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		// STOPPING CONDITION
		if (n->is_terminal && (currentState == _state::up)) {
			result = n;
			return;
		}

	node* parentNode = nullptr;
	parentNode = n->parent;

	// STOPPING CONDITION
	if (parentNode == nullptr) {
		currentState = _state::going_down;
		return;
	}

	node* siblingNode = nullptr;
	_forward_first_sibling(n, siblingNode);

	if (siblingNode == nullptr) {
		_forward_up(parentNode, result);
		return;
	}

	currentState = _state::going_down;
	_forward_down(siblingNode, result);
}

template<typename T>
void trie<T>::base_iterator::_backward_execute(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use base_iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		_backward_up_left(n, result);
}

template<typename T>
void trie<T>::base_iterator::_backward_up_left(node *& n, node *& result) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(n != nullptr, "Do not use a nullptr node")
#endif
		// STOPPING CONDITION
		if (n->is_terminal && (currentState == _state::up)) {
			currentState = _state::left;
			result = n;
			return;
		}

	node* parentNode = nullptr;
	parentNode = n->parent;

	// STOPPING CONDITION
	if (parentNode == nullptr) {
		return;
	}

	node* siblingNode = nullptr;
	_backward_first_sibling(n, siblingNode);

	// STOPPING CONDITION
	if (siblingNode == nullptr) {
		currentState = _state::up;
		_backward_up_left(parentNode, result);
		return;
	}

	currentState = _state::left;
	_backward_first_terminal_descendant(siblingNode, result);
}

template<typename T>
bool trie<T>::base_iterator::operator ==(base_iterator const& rhs) const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(rhs.container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(rhs.container == container, "Do not compare iterators of different containers")
#endif
		return currentNode == rhs.currentNode;
}



template<typename T>
void trie<T>::base_iterator::pre_increment() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (currentNode != nullptr) {
			node* result = nullptr;
			_forward_execute(currentNode, result);
			currentNode = result;
		}
}


template<typename T>
void trie<T>::base_iterator::pre_decrement() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (currentNode == nullptr) {
			currentState = _state::left;
			_backward_first_terminal_descendant(container->root, currentNode);
		}
		else {
			node* result = nullptr;
			_backward_execute(currentNode, result);
			currentNode = result;
		}
}


template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator++() {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (value != nullptr) {
			currentNode->value = std::move(value->second);
			delete value;
			value = nullptr;
		}
	pre_increment();
	return *this;
}

template<typename T>
typename trie<T>::iterator& trie<T>::iterator::operator--() {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (value != nullptr) {
			currentNode->value = std::move(value->second);
			delete value;
			value = nullptr;
		}
	pre_decrement();
	return *this;
}

template<typename T>
typename trie<T>::const_iterator& trie<T>::const_iterator::operator--() {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (value != nullptr) {
			delete value;
			value = nullptr;
		}
	pre_decrement();
	return *this;
}

template<typename T>
typename trie<T>::const_iterator& trie<T>::const_iterator::operator++() {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
		if (value != nullptr) {
			delete value;
			value = nullptr;
		}
	pre_increment();
	return *this;
}

template<typename T>
typename trie<T>::iterator trie<T>::iterator::operator++(int) {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
	iterator copy(*this);
	operator++();
	return copy;
}

template<typename T>
typename trie<T>::iterator trie<T>::iterator::operator--(int) {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
	iterator copy(*this);
	operator--();
	return copy;
}

template<typename T>
typename trie<T>::const_iterator trie<T>::const_iterator::operator++(int) {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
	const_iterator copy(*this);
	operator++();
	return copy;
}

template<typename T>
typename trie<T>::const_iterator trie<T>::const_iterator::operator--(int) {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
#endif
	const_iterator copy(*this);
	operator--();
	return copy;
}


template<typename T>
typename trie<T>::iterator::pointer trie<T>::iterator::operator->() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
	TRIE_ASSERT_DUMP_LOG(currentNode != nullptr, "Do not deference a nullptr")
#endif
		if (value == nullptr) {
			value = new value_type(key_type(accumulated), std::move(currentNode->value));
		}
	return value;
}

template<typename T>
typename trie<T>::iterator::value_type&  trie<T>::iterator::operator*() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(currentNode != nullptr, "Do not deference a nullptr")
#endif
		if (value == nullptr) {
			value = new value_type(key_type(accumulated), std::move(currentNode->value));
		}
	return *value;
}


template<typename T>
typename trie<T>::const_iterator::pointer trie<T>::const_iterator::operator->() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(currentNode != nullptr, "Do not deference a nullptr")
#endif
		if (value == nullptr) {
			value = new value_type(key_type(accumulated), currentNode->value);
		}
	return value;
}

template<typename T>
typename trie<T>::const_iterator::value_type& trie<T>::const_iterator::operator*() const {
#ifdef _DEBUG
	TRIE_LOG;
	TRIE_ASSERT_DUMP_LOG(container != nullptr, "Do not use iterator on no container")
		TRIE_ASSERT_DUMP_LOG(currentNode != nullptr, "Do not deference a nullptr")
#endif
		if (value == nullptr) {
			value = new value_type(key_type(accumulated), currentNode->value);
		}
	return *value;
}


template<typename T>
template<class _RanIt>
template<class _Other>
typename trie<T>::trie_reverse_iterator<_RanIt>& trie<T>::trie_reverse_iterator<_RanIt>::operator=(const trie_reverse_iterator<_Other>& _Right) {
	current = _Right.base();
	return *this;
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt>::reference trie<T>::trie_reverse_iterator<_RanIt>::operator*() const {
	temp._assign(current);
	return *(temp.pre_decrement());
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt>::pointer trie<T>::trie_reverse_iterator<_RanIt>::operator->() const {
	temp._assign(current);
	temp.pre_decrement();
	return (std::_Operator_arrow(temp, std::is_pointer<_RanIt>()));
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt>& trie<T>::trie_reverse_iterator<_RanIt>::operator++() {
	--current;
	return *this;
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt> trie<T>::trie_reverse_iterator<_RanIt>::operator++(int)  {
	trie_reverse_iterator<_RanIt> _tmp = *this;
	--current;
	return _tmp;
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt>& trie<T>::trie_reverse_iterator<_RanIt>::operator--() {
	++current;
	return *this;
}

template<typename T>
template<class _RanIt>
typename trie<T>::trie_reverse_iterator<_RanIt> trie<T>::trie_reverse_iterator<_RanIt>::operator--(int) {
	trie_reverse_iterator<_RanIt> _tmp = *this;
	++current;
	return _tmp;
}


#ifdef _DEBUG

template<typename T>
void trie<T>::base_iterator::_update_log(char * const& func, int const& line, char * const& file) {
	std::stringstream ss;
	ss << "Function: " << func << ", Line: " << line << ", File: " << file << "\n";
	_log.push_back(ss.str());
} // _update_log(char * func, int line, char * file)


template<typename T>
void trie<T>::base_iterator::dump_log(char * const& file) const {
	std::ofstream ofs(file);
	ofs << "Check Count: " << _log.size() << "\n";
	for (auto e : _log)
		ofs << e;

	ofs.close();
} // dump_log(char * file) cons

#endif