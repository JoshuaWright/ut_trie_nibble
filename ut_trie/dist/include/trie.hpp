#if !defined(GUARD_trie_hpp)
#define GUARD_trie_hpp

/**	@file trie.hpp
@author Joshua Wright
@date 2017-05-17
@version 1.0.0
@note Developed for C++11/vc14
@breif trie<T> template definition.
*/


#include <string>
#include <iterator>
#include <array>
#include <memory>
#include <cassert>


#ifdef _DEBUG 
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <functional>
#include <type_traits>
#include <iomanip>
#include <map>

// HELPER MACROS
#define	TRIE_LOG _update_log(__func__ ,__LINE__,__FILE__)
#define TRIE_ASSERT_DUMP_EVERYTHING(expr,msg) if(expr == false) { dump_everything(); assert(expr && msg); }
#define TRIE_ASSERT_DUMP_LOG(expr,msg) if(expr == false) { dump_log(); assert(expr && msg); }
#endif // _DEBUG 


template<typename T>
class trie {
public:

	// forward decorations 
	class iterator;
	class const_iterator;

	template<class _RanIt>
	class trie_reverse_iterator;

	// TYPES
	using mapped_type = T;
	using size_type = std::size_t;
	using key_type = std::string;
	using value_type = std::pair<key_type const, mapped_type>;
	using pointer = value_type*;
	using difference_type = std::ptrdiff_t;
	using reverse_iterator = trie_reverse_iterator<iterator>;
	using const_reverse_iterator = trie_reverse_iterator<const_iterator>;
	using insert_return_type = std::pair<iterator, bool>;

private:

	/*
	Brief: struct to represent trie node
	*/
	struct node {

		// TYPES
		using key_nibble = int;
		using array_type = std::array<node*, 16>;

		// CONTRUCTORS 
		node();
		node(key_nibble);
		node(key_nibble, mapped_type&&);
		node(key_nibble, mapped_type const&);

		// DESTRUCTOR
		~node();

		// MEMBERS	
		bool						is_terminal					= false;
		bool						is_second_nibble			= false;
		size_type					num_terminal_descendants	= 0;
		key_nibble					nibble						= -1;
		node*						parent						= nullptr;
		mapped_type					value;
		array_type					children;

		// FUNCTIONS

		/* Brief: gets the number of children that are not nullptr */
		int	 children_size() const;

		/* Brief: check if a node has children */
		bool has_children()	const { return children_size() != 0; }

		// RELATIONAL OPERATORS 
		bool operator==(node const& n) const;
		bool operator!=(node const& n) const { return !operator==(n); }


#if _DEBUG 
		key_type					key;

		/* Brief: to record all the actions of a trie::node */
		static std::vector<std::string>	_log;

		std::string					_to_string() const;

		/*
		Brief:			dumps the contents of the _log vector to a file
		Parameter 1:	the file
		*/
		void dump_log(char* const& = "trie_node_log.txt") const;

		/*
		Brief:			updates the _log vector
		Parameter 1:	the function
		Parameter 2:	the line
		Parameter 2:	the file
		*/
		static void	_update_log(char* const& func, int const& line, char* const& file);
#endif
	}; // node

public:


	/*
	Brief: base_iterator class for others to derive 
	*/
	class base_iterator :
		public std::iterator<std::bidirectional_iterator_tag, typename trie::value_type>
	{
	private:

		/*
		Brief: enum to help with traversal
		*/
		enum _state {
			down = 0,
			up,
			left,
			going_down,
			going_up
		};


	private:

		// MEMBERS
		mutable node*				currentNode		= nullptr;
		mutable trie<T> const*		container		= nullptr;
		mutable pointer				value			= nullptr;
		mutable _state				currentState;
		mutable key_type			accumulated;

#if _DEBUG
		/* Brief: to record all the actions of a trie::iterator */
		static std::vector<std::string>	_log;
#endif

	public:

		// CONTRUCTORS
		base_iterator();
		base_iterator(trie const*, node*, key_type const&, _state const&);
		base_iterator(trie const* , node* , key_type const& );
		base_iterator(trie const* , node* = nullptr);

		// DESTRUCTOR
		~base_iterator();

		// RELATIONAL OPERATORS 
		bool operator == (base_iterator const& rhs) const;
		bool operator != (base_iterator const& rhs) const { return !operator==(rhs); }

		friend trie;

#ifdef _DEBUG

		/*
		Brief:			dumps the contents of the _log vector to a file
		Parameter 1:	the file
		*/
		void dump_log(char* const& = "trie_iter_log.txt") const;
#endif

	private:

		// HELPERS 

		/* Brief: increment and decrement function to reduce code repletion in derivatives  */
		void pre_decrement() const;
		void pre_increment() const;

		/*
		Brief:			start of forward(++) increment algorithm
		Parameter 1:	the starting base of algorithm
		Parameter 2:	the result
		*/
		void _forward_execute(node*&, node*&) const;

		/*
		Brief:			gets the first terminal descendant going downwards with recursion
		Parameter 1:	the parent
		Parameter 2:	the result
		*/
		void _forward_first_terminal_descendant(node*&, node*&) const;

		/*
		Brief:			gets the first terminal parent or sibling going upwards with recursion
		Parameter 1:	the child
		Parameter 2:	the result
		*/
		void _forward_up(node*&, node*&) const;

		/*
		Brief:			gets the first terminal child going downwards with recursion
		Parameter 1:	the parent
		Parameter 2:	the result
		*/
		void _forward_down(node*&, node*&) const;

		/*
		Brief:			gets the first lesser child thats in not a nullptr
		Parameter 1:	the parent
		Parameter 2:	the result
		*/
		void _forward_first_child(node*&, node*&) const;

		/*
		Brief:			gets the next greater sibling thats in not a nullptr
		Parameter 1:	the sibling
		Parameter 2:	the result
		*/
		void _forward_first_sibling(node*&, node*&) const;

		/*
		Brief:			start of backward(--) decrement algorithm
		Parameter 1:	the starting base of algorithm
		Parameter 2:	the result
		*/
		void _backward_execute(node*&, node*&) const;

		/*
		Brief:			gets the first descendant with children going downwards with recursion
		Parameter 1:	the parent
		Parameter 2:	the result
		*/
		void _backward_first_terminal_descendant(node *&, node*&) const;

		/*
		Brief:			gets the next lesser sibling that is terminal going upwards with recursion
		Parameter 1:	the sibling
		Parameter 2:	the result
		*/
		void _backward_up_left(node*&, node*&) const;

		/*
		Brief:			gets the first greater child thats in not a nullptr
		Parameter 1:	the parent
		Parameter 2:	the result
		*/
		void _backward_first_child(node*&, node*&) const;

		/*
		Brief:			gets the next lesser sibling thats in not a nullptr
		Parameter 1:	the sibling
		Parameter 2:	the result
		*/
		void _backward_first_sibling(node*&, node*&) const;

#ifdef _DEBUG
		/*
		Brief:			updates the _log vector
		Parameter 1:	the function
		Parameter 2:	the line
		Parameter 2:	the file
		*/
		static void	_update_log(char* const& func, int const& line, char* const& file);
#endif
	}; // end class base_iterator


	/*
	Brief: iterator class for trie
	*/
	class iterator : public base_iterator {
	private:
		// MEMBERS
		mutable pointer value = nullptr;
	
		// HELPERS
		void _assign(iterator const&) const;

	public:

		// CONTRUCTORS
		iterator();
		iterator(iterator const&);
		iterator(trie const*, node*, key_type const&);
		iterator(trie const*, node* = nullptr);

		// DESTRUCTOR
		~iterator();

		// MEMBER ACCESS
		pointer		operator->() const;
		value_type&	operator*() const;

		// INCREMENT / DECREMENT 
		iterator&	operator++();
		iterator	operator++(int);
		iterator&	operator--();
		iterator	operator--(int);

		// ASSIGNMENT 
		iterator& operator=(iterator const&);

		template<typename T>
		friend class trie_reverse_iterator;

	}; // iterator


	/*
	Brief: const_iterator class for trie
	*/
	class const_iterator : public base_iterator {
	private:
		// MEMBERS
		mutable pointer value = nullptr;

		// HELPERS
		void _assign(const_iterator const&) const;

	public:
		
		// CONTRUCTORS
		const_iterator();
		const_iterator(const_iterator const&);
		const_iterator(trie const* , node* , key_type const&);
		const_iterator(trie const* , node*  = nullptr);

		// DESTRUCTOR
		~const_iterator();

		// MEMBER ACCESS
		pointer		operator->() const;
		value_type&	operator*() const;

		// INCREMENT / DECREMENT 
		const_iterator&	operator++();
		const_iterator	operator++(int);
		const_iterator&	operator--();
		const_iterator	operator--(int);

		// ASSIGNMENT 
		const_iterator& operator=(iterator const&);

		template<typename T>
		friend class trie_reverse_iterator;

	}; // const_iterator

	/*
	Brief: reverse_iterator template class for trie
	*/
	template<class _RanIt>
	class trie_reverse_iterator :
		public std::iterator<
		typename std::iterator_traits<_RanIt>::iterator_category,
		typename std::iterator_traits<_RanIt>::value_type,
		typename std::iterator_traits<_RanIt>::difference_type,
		typename std::iterator_traits<_RanIt>::pointer,
		typename std::iterator_traits<_RanIt>::reference>
	{
	private:

		// TYPES
		using _Myt = trie_reverse_iterator<_RanIt>;

		// MEMBERS
		_RanIt current;
		_RanIt temp;

	public:

		// TYPES
		using difference_type = typename std::iterator_traits<_RanIt>::difference_type;
		using pointer = typename std::iterator_traits<_RanIt>::pointer;
		using reference = typename std::iterator_traits<_RanIt>::reference;
		using iterator_type = _RanIt;

		// CONSTRUCTORS 
		trie_reverse_iterator() : current() {}
		explicit trie_reverse_iterator(_RanIt _Right) : current(_Right) {}

		template<class _Other>
		trie_reverse_iterator(const trie_reverse_iterator<_Other>& _Right) : current(_Right.base()) {}

		template<class _Other>
		trie_reverse_iterator<_RanIt>& operator=(const trie_reverse_iterator<_Other>&);


		// OPERATORS
		_RanIt base() const { return current; }

		// MEMBER ACCESS
		reference operator*() const;
		pointer operator->() const;

		// INCREMENT / DECREMENT
		trie_reverse_iterator<_RanIt>& operator++();
		trie_reverse_iterator<_RanIt> operator++(int);
		trie_reverse_iterator<_RanIt>& operator--();
		trie_reverse_iterator<_RanIt> operator--(int);

		// RELATIONAL
		bool operator == (_Myt& _Right) const { return (base() == _Right.base()); }
		bool operator != (_Myt& _Right) const { return !operator==(_Right); }

	}; // reverse_iterator


private:

	// MEMBERS
	mutable node* root;

#if _DEBUG
	/* Brief: to record all the actions of a trie */
	static std::vector<std::string>	_log;
#endif

public:

	// CONTRUCTORS / DESTRUCTOR
	trie();
	trie(trie const&);
	trie(trie&& other);
	trie(std::initializer_list<value_type> const&);

	// iterator range ctor
	template< class InputIterator >
	trie(InputIterator first, InputIterator last);

	// DESTRUCTOR
	~trie();

	// ELEMENT ACCESS
	mapped_type&	operator[](key_type const&);
	value_type		front() { return *begin(); }
	value_type		back() { return *(--end()); }

	// ASSIGNMENT 
	trie&	operator=(trie const&);

	// assignment move 
	trie&	operator=(trie &&);
	trie&	operator=(std::initializer_list<value_type> const&);

	// MODIFIERS
	void				clear();

	// insert rValue
	insert_return_type	insert(value_type&&);

	insert_return_type	insert(value_type const&);
	void				insert(std::initializer_list<value_type> const&);

	// insert iterator range
	template< class InputIt >
	void insert(InputIt first, InputIt last);

	void				swap(trie<T>&);
	iterator			erase(iterator const&);
	size_type			erase(key_type const&);

	// erase range 
	iterator			erase(iterator const&, iterator const&);

	// ITERATORS
	iterator				begin();
	iterator				end() { return iterator(this); }
	const_iterator			begin()		const { return cbegin(); }
	const_iterator			end()		const { return cend(); }
	const_iterator			cbegin()	const;
	const_iterator			cend()		const { return const_iterator(this); }

	reverse_iterator		rbegin() { return reverse_iterator(end()); }
	reverse_iterator		rend() { return reverse_iterator(begin()); }
	const_reverse_iterator	rbegin()	const { return crbegin(); }
	const_reverse_iterator	rend()		const { return crend(); }
	const_reverse_iterator	crbegin()	const { return const_reverse_iterator(cend()); }
	const_reverse_iterator	crend()		const { return const_reverse_iterator(cbegin()); }

	// CAPACITY
	size_type	size()		const { return root->num_terminal_descendants; }
	bool		empty()		const { return size() == 0; }
	size_type	max_size()	const { return std::numeric_limits<size_type>::max(); }

	// LOOKUP
	size_type	count(key_type const& key)const;
	iterator	find(key_type const& key)const;

	// RELATIONAL 
	bool operator ==	(trie const&)		const;
	bool operator >		(trie const&)		const;
	bool operator !=	(trie const& rhs)	const { return !operator==(rhs); }
	bool operator <		(trie const& rhs)	const { return rhs.operator>(*this); }
	bool operator >=	(trie const& rhs)	const { return !operator<(rhs); }
	bool operator <=	(trie const& rhs)	const { return !operator>(rhs); }

#ifdef _DEBUG
	/*
	Brief:			dumps the log and the all the nodes into a file
	Parameter 1:	the file
	*/
	void dump_everything(char* const& = "trie_log.txt") const;

	/*
	Brief:			dumps the log
	Parameter 1:	the file
	*/
	void dump_log(char* const& = "trie_log.txt") const;
#endif

private:

	// TYPES 
	using byte = std::pair<int, int>;

	// HELPERS

	/*
	Brief:			finds a node
	Parameter 1:	the key
	return:			the node | nullptr
	*/
	node*	_find_node(key_type const&) const;

	/*
	Brief:			finds a node recursively
	Parameter 1:	the key
	Parameter 2:	the index of the key
	Parameter 3:	the result
	*/
	void	_find_node_loop_recursion(node*&, key_type const&, int&, node*&) const;

	/*
	Brief:			finds a element recursively
	Parameter 1:	the key
	Parameter 2:	the index of the key
	Parameter 3:	the result
	*/
	void	_find_loop_recursion(node* n, key_type const&, int&, iterator&) const;

	/*
	Brief:			rVale inserts a value starting directly from the root
	Parameter 1:	the base
	Parameter 2:	the index of the key
	Parameter 3:	the value
	Parameter 4:	the location of insertion
	*/
	void	_insert_from_root(node*, int&, value_type &&, insert_return_type&);

	/*
	Brief:			rVale inserts a value starting from a value present in already
	Parameter 1:	the base
	Parameter 2:	the index of the key
	Parameter 3:	the value
	Parameter 4:	the location of insertion
	*/
	void	_insert_from_under_root(node*, int&, value_type &&, insert_return_type&);

	/*
	Brief:			inserts a value starting directly from the root
	Parameter 1:	the base
	Parameter 2:	the index of the key
	Parameter 3:	the value
	Parameter 4:	the location of insertion
	*/
	void	_insert_from_root(node*, int&, value_type const&, insert_return_type&);

	/*
	Brief:			inserts a value starting from a value present in already
	Parameter 1:	the base
	Parameter 2:	the index of the key
	Parameter 3:	the value
	Parameter 4:	the location of insertion
	*/
	void	_insert_from_under_root(node*, int&, value_type const&, insert_return_type&);

	/*
	Brief:			removes children recursively
	Parameter 1:	the parent
	*/
	void	_clear_loop_recursion(node*);

	/*
	Brief:			erases if there are no children
	Parameter 1:	the element to be eased
	Parameter 2:	next the element
	*/
	void	_erase_loop_recursion(iterator&, iterator&);

	/*
	Brief:			inserts a two nibbles
	Parameter 1:	the parent
	Parameter 2:	the character both nibbles represent
	return:			the second nibble
	*/
	node*	_insert(node*, char const&);

	/*
	Brief:			gets the second nibble of that character
	Parameter 1:	the parent
	Parameter 2:	the character both nibbles represent
	return:			the second nibble | nullptr
	*/
	static node*			_get(node*, char const&);

	/*
	Brief:			test if a parent represents other half of character
	Parameter 1:	the child
	Parameter 2:	the character both nibbles may represent
	*/
	static bool				_equal_from_second_nibble(node*, char const&);

	/*
	Brief:			translates two nibbles into a character
	Parameter 1:	the first nibble
	Parameter 2:	the second nibble
	*/
	static char				_translate(int const& first, int const& second) { return static_cast<unsigned char>((first * 16) + second); }

	/*
	Brief:			translates a byte to a character
	Parameter 1:	the character
	*/
	static unsigned char	_translate(byte const& b) { return static_cast<unsigned char>((b.first * 16) + b.second); }

	/*
	Brief:			translates a character to a byte
	Parameter 1:	the byte
	*/
	static byte				_translate(unsigned char const& c) { return byte(c / 16, c % 16); }

#ifdef _DEBUG
	/*
	Brief:			dumps all the children to a file
	Parameter 1:	the parent
	Parameter 2:	the file-object
	*/
	void	_dump_children(node*, std::ofstream&) const;

	/*
	Brief:			updates the _log vector
	Parameter 1:	the function
	Parameter 2:	the line
	Parameter 3:	the file
	*/
	static void	_update_log(char* const& func, int const& line, char* const& file);
#endif

}; // end class


   //IMPLEMENTATIONS 
#include"trie_imp.hpp"
#include"node_imp.hpp"
#include"iterator_imp.hpp"

#endif // GUARD_trie_hpp