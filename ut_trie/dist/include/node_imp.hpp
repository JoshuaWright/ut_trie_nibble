#pragma once

/**	@file node_imp.hpp
	@author Joshua Wright
	@date 2017-06-11
	@version 1.0.0
	@note Developed for C++11/vc14
	@breif trie<T>::node template implementations.
	*/

#include "trie.hpp"


#ifdef _DEBUG
template<typename T>
std::vector<std::string> trie<T>::node::_log;
#endif


template<typename T>
trie<T>::node::node() {
#ifdef _DEBUG 
	TRIE_LOG;
#endif
	children.fill(nullptr); 
}

template<typename T>
trie<T>::node::node(key_nibble n) : nibble(n), value(mapped_type()){
#ifdef _DEBUG 
	TRIE_LOG;
#endif
	children.fill(nullptr);
}

template<typename T>
trie<T>::node::node(key_nibble n, mapped_type&& m) :nibble(n), value(std::move(m)), is_terminal(true) {
#ifdef _DEBUG 
	TRIE_LOG;
#endif
	children.fill(nullptr);
}

template<typename T>
trie<T>::node::node(key_nibble n, mapped_type const& m) : nibble(n), value(m), is_terminal(true) {
#ifdef _DEBUG 
	TRIE_LOG;
#endif
	children.fill(nullptr);
}

template<typename T>
trie<T>::node::~node() {
#ifdef _DEBUG 
	TRIE_LOG;
#endif
}



template<typename T>
int trie<T>::node::children_size() const {
	int not_null_count = 0;
	for (typename array_type::size_type i = 0; i < children.size(); ++i) {
		if (children[i] != nullptr)
			++not_null_count;
	}
	return not_null_count;
}

template<typename T>
bool trie<T>::node::operator==(node const & rhs) const {
	if (is_terminal == rhs.is_terminal &&
		num_terminal_descendants == rhs.num_terminal_descendants &&
		value == rhs.value &&
		nibble == rhs.nibble &&
		parent == rhs.parent &&
		children_size() == rhs.children_size())
		return true;
	return false;
}


#ifdef _DEBUG

template<typename T>
std::string trie<T>::node::_to_string() const {
	std::stringstream ss;
	ss << "nibble: " << nibble;

	if(!key.empty())
		ss << "\nkey: " << key;

	ss	<< "\nis terminal: "<<  std::boolalpha <<  is_terminal 
		<< "\nterminal descendants: " << num_terminal_descendants
		<<  "\nis second nibble: " << std::boolalpha  <<  is_second_nibble 
		<< "\nnumber of children: " << children_size() 
		<< "\n";
	return ss.str();
}


template<typename T>
void trie<T>::node::dump_log(char* const& file) const {
	assert(root != nullptr && "root should never be nullptr");
	std::ofstream ofs(file);
	ofs << "Check Count: " << _log.size() << "\n";
	for (auto e : _log)
		ofs << e;

	ofs.close();
} // _dump_eveything()


template<typename T>
void trie<T>::node::_update_log(char * const& func, int const& line, char * const& file) {
	std::stringstream ss;
	ss << "Function: " << func << ", Line: " << line << ", File: " << file << "\n";
	_log.push_back(ss.str());
} // _update_log(char * func, int line, char * file)

#endif